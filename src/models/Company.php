<?php

namespace quoma\company\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property integer $company_id
 * @property string $name
 * @property string $status
 * @property string $tax_identification
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property integer $parent_id
 * @property string $certificate
 * @property string $key
 * @property string $create_timestamp
 * @property string $iibb
 * @property date $start
 * @property string $fantasy_name
 * @property string $certificate_phrase
 * @property string $code
 *
 * @property Company $parent
 * @property Company[] $companies
 */
class Company extends \quoma\core\db\ActiveRecord
{

     /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_company');
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_timestamp'],
                ],
                'value' => function(){return time();}
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'tax_condition_id', 'tax_identification',], 'required'],
            [['name', 'address', 'email', 'fantasy_name', 'certificate_phrase'], 'string', 'max' => 255],
            [['status'], 'in', 'range' => ['enabled', 'disabled']],
            [['parent_id', 'tax_condition_id'], 'integer'],
            [['tax_identification', 'phone', 'iibb'], 'string', 'max' => 45],
            [['code'], 'string', 'max' => 4],
            [['certificate'], 'file', 'extensions' => 'crt'],
            [['key'], 'file', 'extensions' => 'key'],
            [['start'], 'date'],
            [['default'], 'boolean'],
            [['email'], 'email'],
            [['logo'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'fantasy_name' => Yii::t('app', 'Fantasy Name'),
            'status' => Yii::t('app', 'Status'),
            'tax_identification' => Yii::t('app', 'Tax Identification'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'parent_id' => Yii::t('app', 'Parent'),
            'certificate' => Yii::t('app', 'Certificate'),
            'key' => Yii::t('app', 'Key'),
            'parent' => Yii::t('app', 'Parent'),
            'taxCondition' => Yii::t('app', 'Tax Condition'),
            'iibb' => Yii::t('app', 'IIBB'),
            'start' => Yii::t('app', 'Company Start Date'),
            'default' => Yii::t('app', 'Default'),
            'logo' => Yii::t('app', 'Logo'),
            'certificate_phrase' => Yii::t('app', 'Certificate Phrase'),
            'code' => Yii::t('app', 'Entity Payment Code'),
        ];
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['parent_id' => 'company_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointsOfSale()
    {
        return $this->hasMany(PointOfSale::className(), ['company_id' => 'company_id']);
    }
    
    /**
     * Devuelve el pto de venta por defecto
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultPointOfSale()
    {
        
        return $this->getPointsOfSale()->where(['default' => 1])->one();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxCondition()
    {
        return $this->hasOne(TaxCondition::className(), ['tax_condition_id' => 'tax_condition_id']);
    }

        
    /**
     * @inheritdoc
     * Strong relations: Parent.
     */
    public function getDeletable()
    {
        if($this->getCompanies()->exists()){
            return false;
        }
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: .
     */
    protected function unlinkWeakRelations(){
        
        $this->unlinkAll('pointsOfSale', true);

    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Solo un default (habilitado)
     * @param type $insert
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){

            if($this->default && $this->status == 'enabled'){
                Company::updateAll(['default' => 0], ['status' => 'enabled']);
            }
            $this->formatDatesBeforeSave();
            return true;

        }else{
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->formatDatesAfterFind();
        parent::afterFind();
    }

    /**
     * @brief Format dates using formatter local configuration
     */
    private function formatDatesAfterFind()
    {
        $this->start = Yii::$app->formatter->asDate($this->start);
    }

    /**
     * @brief Format dates as database requieres it
     */
    private function formatDatesBeforeSave()
    {
        $this->start = Yii::$app->formatter->asDate($this->start, 'yyyy-MM-dd');
    }

    public static function findDefault()
    {
        $company = Company::find()->where(['default' => 1, 'status' => 'enabled'])->one();
        if($company === null){
            throw new \yii\web\HttpException(500, 'Default company not defined.');
        }
        return $company;
    }
    
    public function getLogoWebPath()
    {
        return (!$this->logo ? null : 'uploads/logos/'.basename($this->logo) );
    }
}
