<?php

namespace quoma\company;

use yii\base\BootstrapInterface;
use quoma\core\menu\Menu;
use quoma\core\module\QuomaModule;
/**
 * Description of CompanyModule
 *
 * @author Gabriela
 */
class CompanyModule extends QuomaModule implements BootstrapInterface {
    
    public $controllerNamespace = 'quoma\company\controllers';
    
    public function init() {
        
        parent::init();

        $this->registerTranslations();
        
        \Yii::$app->setAliases(['@quoma'=> $this->basePath]);
    }

    public function getMenu(Menu $menu)
    {
        $_menu = (new Menu(Menu::MENU_TYPE_ROOT))
            ->setName('company')
            ->setLabel(self::t('Company'))
            ->setSubItems([
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Sites'))->setUrl(['/company/company/index']),
//                (new Menu(Menu::MENU_TYPE_DIVIDER)),
//                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Platforms'))->setUrl(['/checkout_online/platform/index']),
//                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Payment Methods'))->setUrl(['/checkout_online/payment-method/index']),
//                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Installments'))->setUrl(['/checkout_online/installment/index']),
            ])
        ;
        $menu->addItem($_menu, Menu::MENU_POSITION_LAST);
        return $_menu;
    }

    public function getDependencies()
    {
        return [
        ];
    }
}
