<?php

use yii\db\Migration;

class m170515_131920_create_tables extends Migration {

    public function up() {

        $db = quoma\core\helpers\DbHelper::getDbName('db_company');
        
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`tax_condition` (
            `tax_condition_id` INT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(45) NULL DEFAULT NULL,
            `exempt` TINYINT(1) NULL DEFAULT NULL,
            PRIMARY KEY (`tax_condition_id`))
          ENGINE = InnoDB
          DEFAULT CHARACTER SET = latin1;");

        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`company` (
            `company_id` INT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NULL DEFAULT NULL,
            `status` ENUM('enabled', 'disabled') NULL DEFAULT NULL,
            `tax_identification` VARCHAR(45) NULL DEFAULT NULL,
            `address` VARCHAR(255) NULL DEFAULT NULL,
            `phone` VARCHAR(45) NULL DEFAULT NULL,
            `email` VARCHAR(255) NULL DEFAULT NULL,
            `parent_id` INT(11) NULL DEFAULT NULL,
            `certificate` VARCHAR(255) NULL DEFAULT NULL,
            `key` VARCHAR(255) NULL DEFAULT NULL,
            `create_timestamp` INT(11) NULL DEFAULT NULL,
            `tax_condition_id` INT(11) NOT NULL,
            `start` DATE NULL DEFAULT NULL,
            `iibb` VARCHAR(45) NULL DEFAULT NULL,
            `default` TINYINT(1) NULL DEFAULT NULL,
            PRIMARY KEY (`company_id`),
            CONSTRAINT `fk_company_company1`
              FOREIGN KEY (`parent_id`)
              REFERENCES `$db`.`company` (`company_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
            CONSTRAINT `fk_company_tax_condition1`
              FOREIGN KEY (`tax_condition_id`)
              REFERENCES `$db`.`tax_condition` (`tax_condition_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION)
          ENGINE = InnoDB
          DEFAULT CHARACTER SET = utf8;");

        $this->execute("CREATE INDEX `fk_company_company1_idx` ON `$db`.`company` (`parent_id` ASC);");
        $this->execute("CREATE INDEX `fk_company_tax_condition1_idx` ON `$db`.`company` (`tax_condition_id` ASC);");
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`point_of_sale` (
            `point_of_sale_id` INT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(45) NOT NULL,
            `number` INT(11) NOT NULL,
            `status` ENUM('enabled', 'disabled') NULL DEFAULT NULL,
            `description` VARCHAR(255) NULL DEFAULT NULL,
            `company_id` INT(11) NOT NULL,
            `default` TINYINT(1) NULL DEFAULT NULL,
            PRIMARY KEY (`point_of_sale_id`),
            CONSTRAINT `fk_sale_point_company1`
              FOREIGN KEY (`company_id`)
              REFERENCES `$db`.`company` (`company_id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION)
          ENGINE = InnoDB
          DEFAULT CHARACTER SET = utf8;");

        $this->execute("CREATE INDEX `fk_point_of_sale_company1_idx` ON `$db`.`point_of_sale` (`company_id` ASC);");
    }

    public function down() {
        echo "m170515_131920_create_tables cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
